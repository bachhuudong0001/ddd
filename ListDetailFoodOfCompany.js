import React, { PureComponent } from "react";
import { View, StyleSheet, Text, ImageBackground, Animated, ScrollView, Image, FlatList } from "react-native";
import Header from '@src/components/ui/Header'
import * as utils from '@src/utils'
import StarRate from "@src/components/ui/StarRate";
import Button from '@src/components/ui/Button'
HEADER_MAX_HEIGHT = utils.scale(400)
HEADER_MIN_HEIGHT = 0
HEADER_TITLE_HEIGHT = utils.scale(200)
const dataCompany = require('@src/assets/database/dataCompany.json');
class ListDetailFoodOfCompany extends PureComponent {
    render() {
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp'
        })
        const headerTitleBottom = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0, -HEADER_TITLE_HEIGHT / 2],
            extrapolate: 'clamp'
        })
        const contentMarginTop = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0, HEADER_TITLE_HEIGHT / 2],
            extrapolate: 'clamp'
        })
        const headerZindex = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0, 1000],
            extrapolate: 'clamp'
        })
        const headerColorTabBottom = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_TITLE_HEIGHT / 2, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: ['rgba(0,0,0,0.4)', 'rgba(255,255,255,0)', 'rgba(255,255,255,1)'],
            extrapolate: 'clamp'
        })
        const opacityHeaderNameBottom = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [1, 0],
            extrapolate: 'clamp'
        })
        return (
            <View style={styles.container} >
                <Header
                    iconLeft={'menu'}
                    iconRight={''}
                />
                <Animated.View style={{
                    position: 'absolute',
                    top: utils.scale(100),
                    left: 0,
                    right: 0,
                    height: headerHeight,
                    zIndex: headerZindex,
                }}>
                    <Image source={require('@src/assets/images/imageHome.jpg')}
                        style={{ flex: 1, width: '100%', height: 100 }}
                    />
                    <Animated.View style={{
                        position: 'absolute',
                        bottom: headerTitleBottom,
                        height: HEADER_TITLE_HEIGHT,
                        width: '100%',
                        alignItems: 'center',
                    }}>
                        <Animated.View opacity={opacityHeaderNameBottom} style={{
                            flexDirection: 'row',
                            position: 'absolute',
                            marginHorizontal: utils.scale(15),
                            alignItems: 'center'
                        }}>
                            <Text numberOfLines={1} style={{
                                flex: 1,
                                color: 'white',
                                fontSize: utils.scale(46)
                            }}>FIT FOOD </Text>
                            <View style={{ alignItems: 'flex-end', flex: 1, }}>
                                <Text numberOfLines={1} style={{ color: 'white' }}>Healthy food </Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <StarRate rate={4} />
                                    <Text numberOfLines={1} style={{ color: 'white', fontSize: utils.scale(22) }}>(7)</Text>
                                </View>
                            </View>
                        </Animated.View>
                        <Animated.View style={{
                            flexDirection: 'row',
                            position: 'absolute',
                            bottom: 0,
                            height: HEADER_TITLE_HEIGHT / 2,
                            width: '100%',
                            padding: 5,
                            alignItems: 'center',
                            backgroundColor: headerColorTabBottom
                        }}>
                            <Button
                                title='Menu'
                                styles={{
                                    flex: 1,
                                    backgroundColor: 'red'
                                }}
                            />
                            <Button
                                title='Feedback'
                                colorText='red'
                                styles={{
                                    flex: 1,
                                    backgroundColor: 'transparent',
                                    borderWidth: 1,
                                    borderColor: 'red'
                                }}
                            />
                            <Button
                                title='Infomation'
                                colorText='red'
                                styles={{
                                    flex: 1,
                                    backgroundColor: 'transparent',
                                    borderWidth: 1,
                                    borderColor: 'red'
                                }}
                            />
                        </Animated.View>
                    </Animated.View>
                </Animated.View>
                <Animated.FlatList
                    style={{ marginTop: contentMarginTop, }}
                    contentContainerStyle={{
                        paddingTop: HEADER_MAX_HEIGHT + utils.scale(30)
                    }}
                    data={dataCompany}
                    keyExtractor={item => item.id.toString()}
                    renderItem={({ item, index }) => {
                        return (
                            <View style={{
                                height: 100,
                                backgroundColor: item.id % 2 ? 'blue' : 'red'
                            }}>
                                <Text style={{
                                }}>{item.nameCompany}</Text>
                            </View>
                        )
                    }
                    }
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
                    )}
                />
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#b1b1b1'
    },
    image: {
        width: '100%',
        height: utils.deviceHeight / 2
    },
});

export default ListDetailFoodOfCompany;
