import * as type from '../actionTypes';

export const findLocationFoodCompanyRequest = (payload) => ({
    type: type.FIND_LOCATION_FOOD_COMPANY_REQUEST,
    payload
});
export const findLocationFoodCompanySuccess = (result) => ({
    type: type.FIND_LOCATION_FOOD_COMPANY_SUCCESS,
    result
});
export const findLocationFoodCompanyFailed = (error) => ({
    type: type.FIND_LOCATION_FOOD_COMPANY_FAILED,
    error
});

export const listLocationFoodCompanyRequest = (payload) => ({
    type: type.LIST_LOCATION_FOOD_COMPANY_REQUEST,
    payload
});
export const listLocationFoodCompanySuccess = (result) => ({
    type: type.LIST_LOCATION_FOOD_COMPANY_SUCCESS,
    result
});
export const listLocationFoodCompanyFailed = (error) => ({
    type: type.LIST_LOCATION_FOOD_COMPANY_FAILED,
    error
});
export const resetListLocationFoodCompany = () => ({
    type: type.RESET_LIST_LOCATION_FOOD_COMPANY
});

