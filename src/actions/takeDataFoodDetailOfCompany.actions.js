import * as type from '../actionTypes';

export const takeDataDetailFoodCompanyRequest = (payload) => ({
    type: type.TAKE_DATA_DETAIL_FOOD_COMPANY_REQUEST,
    payload
});
export const takeDataDetailFoodCompanySuccess = (result) => ({
    type: type.TAKE_DATA_DETAIL_FOOD_COMPANY_SUCCESS,
    result
});
export const takeDataDetailFoodCompanyFailed = (error) => ({
    type: type.TAKE_DATA_DETAIL_FOOD_COMPANY_FAILED,
    error
});
export const resetTakeDataDetailFoodCompany = () => ({
    type: type.RESET_TAKE_DATA_DETAIL_FOOD_COMPANY
});
