import React from "react";
import CreateStackNavigator from "./tabNavigator/createStackNavigator";
import {
  Provider,
} from 'react-redux';
import store from './configs/store.config';

const App = () => (
  <Provider store={store}>
    <CreateStackNavigator />
  </Provider>
);

export default App;