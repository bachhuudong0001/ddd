import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '@src/actions';
import * as selectors from '@src/selectors';
import HomeScreen from "./HomeScreen";
import { Animated, Easing, Keyboard } from "react-native";

class HomeScreenContainer extends HomeScreen {
    constructor(props) {
        super(props);
        this.state = {
            showKeyboard: false, //check hide show keyboard
            time: new Animated.Value(0),// time animation for input and button
            timeForIconMapPin: new Animated.Value(0), //time animation for button "Map-Pin"
            inputDisAction: false,// dis action amination for input before init app
            timeKeyboard: new Animated.Value(0), //time animation for input when hide or show keyboard
        }
    }
    componentDidMount() {
        //setup animation when start app
        const time1 = Animated.timing(
            this.state.time,
            {
                toValue: 3,
                duration: 900,
                easing: Easing.ease
            }
        );
        const time2 = Animated.spring(
            this.state.timeForIconMapPin,
            {
                toValue: 1,
                duration: 100
            }
        )
        Animated.sequence([time1, time2]).start();// time1 done -> time2
    }
    componentWillMount() {
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.keyboardAction(false));
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this.keyboardAction(true));
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    changeAddress = (adress) => {
        this.props.findLocationFoodCompanyRequest(adress);//find address user want to there..
    }
    gotoDetail = (data) => {
        this.props.navigation.navigate('ListCompanyFoodScreen', { data });
    }
    onFocusInput = () => {
        this.setState({
            showKeyboard: true
        })
    }
    keyboardAction = (statusShow) => {
        this.setState({
            showKeyboard: statusShow,
            inputDisAction: true //when focus input action amination input will dis
        }, () => {
            statusShow ? null : Keyboard.dismiss();

            Animated.spring(
                this.state.timeKeyboard,
                {
                    toValue: statusShow ? 1 : 0,
                    duration: 100
                }
            ).start();

            const time1 = Animated.timing(
                this.state.time,
                {
                    toValue: statusShow ? 0 : 3,
                    duration: 600,
                    easing: Easing.ease
                }
            );
            const time2 = Animated.spring(
                this.state.timeForIconMapPin,
                {
                    toValue: statusShow ? 0 : 1,
                    duration: 100
                }
            );
            Animated.sequence([time1, time2]).start();
        })
    }
}

const mapStateToProps = (state) => {
    return {
        getDataListRecommend: selectors.getDataListRecommend(state),
        getDataInfoLocaionUser: selectors.getDataInfoLocaionUser(state),
        getDataListFoodCompanySearchByname: selectors.getDataListFoodCompanySearchByname(state),
        getIsLoaddingListFoodCompanySearchByname: selectors.getIsLoaddingListFoodCompanySearchByname(state),
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreenContainer);
