import React, { PureComponent } from "react";
import { View, StyleSheet, Text, ImageBackground, Animated } from "react-native";
import Header from '@src/components/ui/Header'
import BodyHome from '@src/components/ui/BodyHome'
import ListHorizontal from '@src/components/ui/ListHorizontal'
import ListResultAdress from '@src/components/ui/ListResultAdress'
import * as utils from '@src/utils'
class HomeScreen extends PureComponent {
    render() {
        let { showKeyboard } = this.state;
        const topHorizontalList = this.state.time.interpolate({
            inputRange: [0, 1, 2, 3],
            outputRange: [500, 0, 0, 0]
        })
        const marginRightInput = this.state.time.interpolate({
            inputRange: [0, 1, 2, 3],
            outputRange: [-1000, -1000, 0, 0]
        })
        const marginLeftButton = this.state.time.interpolate({
            inputRange: [0, 1, 2, 3],
            outputRange: [-1000, -1000, -1000, 0]
        })

        const bottomIcon = this.state.timeForIconMapPin.interpolate({
            inputRange: [0, 1],
            outputRange: [-1000, -utils.scale(32.5)]
        })
        const marginTopForInput = this.state.timeKeyboard.interpolate({
            inputRange: [0, 1],
            outputRange: [utils.scale(250), 0]
        })
        return (
            <View style={styles.container}>
                <ImageBackground source={require('@src/assets/images/imageHome.jpg')}
                    style={{
                        ...styles.image
                    }}
                    opacity={showKeyboard ? 0.7 : 0.85}>
                    <Header
                        iconLeft={showKeyboard ? null : 'menu'}
                        textLeft={showKeyboard ? 'Cancel' : null}
                        textRight={showKeyboard ? 'Done' : null}
                    />
                    <BodyHome
                        marginTopForInput={marginTopForInput}
                        marginRightInput={marginRightInput}
                        marginLeftButton={marginLeftButton}
                        bottomIcon={bottomIcon}
                        showKeyboard={showKeyboard}
                        inputDisAction={this.state.inputDisAction}
                        changeAddress={this.changeAddress}
                        onFocusInput={this.onFocusInput}
                    />
                </ImageBackground>
                {
                    showKeyboard ?
                        <Animated.View style={{ ...styles.listResultSearch }}>
                            <ListResultAdress
                                gotoDetail={this.gotoDetail}
                                dataMyLocation={this.props.getDataInfoLocaionUser}
                                dataResultLocation={this.props.getDataListFoodCompanySearchByname}
                                getIsLoaddingListFoodCompanySearchByname={this.props.getIsLoaddingListFoodCompanySearchByname}
                            />
                        </Animated.View>
                        :
                        <Animated.View style={{
                            flex: 1,
                            marginTop: topHorizontalList
                        }} >
                            <Text style={styles.txtTitle}>We recommend</Text>
                            <ListHorizontal
                                data={this.props.getDataListRecommend}
                            />
                        </Animated.View>
                }
            </View >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f1f1'
    },
    image: {
        width: '100%',
        height: utils.deviceHeight / 1.55
    },
    input: {
        height: utils.scale(100),
        backgroundColor: 'white',
        width: utils.deviceWidth - utils.scale(80),
        paddingVertical: 0,
        paddingHorizontal: utils.scale(20),
        backgroundColor: 'white',
        borderBottomLeftRadius: utils.scale(10),
        borderBottomRightRadius: utils.scale(10)
    },
    txtTitle: {
        color: '#111111',
        fontSize: utils.scale(36),
        marginLeft: utils.scale(20),
        marginTop: utils.scale(20)
    },
    listResultSearch: {
        backgroundColor: 'white',
        height: utils.scale(450),
        width: '100%',
        position: 'absolute',
        bottom: 0,
    }
});

export default HomeScreen;
