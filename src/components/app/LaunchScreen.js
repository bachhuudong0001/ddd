import React, { PureComponent } from "react";
import { StyleSheet, Text, ActivityIndicator, ImageBackground, View } from "react-native";
import * as utils from '@src/utils'
import Header from '@src/components/ui/Header'
class LaunchScreen extends PureComponent {
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.replace('HomeScreen');
        }, 0);
    }
    render() {
        return (
            <ImageBackground source={require('@src/assets/images/imageFood.jpg')}
                style={{ ...styles.image }}>
                <View style={{ ...styles.viewWelcome }}>
                    <Text style={{ ...styles.txt }}>WELCOME FOOD APP</Text>
                    <ActivityIndicator size={'small'} color={'white'} />
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    txt: {
        fontSize: utils.scale(40),
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        marginBottom: utils.scale(20)
    },
    image: {
        width: null,
        height: null,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewWelcome: {
        backgroundColor: 'red',
        margin: utils.scale(30),
        paddingVertical: utils.scale(30),
        paddingHorizontal: utils.scale(20),
        borderRadius: utils.scale(20)
    }
});

export default LaunchScreen;
