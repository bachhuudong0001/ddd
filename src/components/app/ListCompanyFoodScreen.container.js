import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '@src/actions';
import * as selectors from '@src/selectors';
import ListCompanyFoodScreen from "./ListCompanyFoodScreen";

class ListCompanyFoodScreenContainer extends ListCompanyFoodScreen {
    constructor(props) {
        super(props);
        this.page = 0;
    }
    componentDidMount() {
        this.loadMoreDataListFoodCompany();
    }
    loadMoreDataListFoodCompany = () => {
        //no page.. so just do it for fun =)))
        if (this.page == 1) return;
        this.props.listLocationFoodCompanyRequest(this.props.navigation.getParam('data'));
        this.page++;
    }
    componentWillUnmount() {
        this.props.resetListLocationFoodCompany();
    }
    gotoDetailFood = (data) => {
        this.props.navigation.navigate('ListDetailFoodOfCompany', { data });
    }
}

const mapStateToProps = (state) => {
    return {
        getDataListFoodCompany: selectors.getDataListFoodCompany(state),
        getIsLoaddingListFoodCompany: selectors.getIsLoaddingListFoodCompany(state),

    };
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(ListCompanyFoodScreenContainer);
