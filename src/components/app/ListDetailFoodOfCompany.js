import React, { PureComponent } from "react";
import { View, StyleSheet, Animated, FlatList, ActivityIndicator } from "react-native";
import Header from '@src/components/ui/Header'
import * as utils from '@src/utils'
import HeaderListDetailFoodOfCompany from '@src/components/ui/HeaderListDetailFoodOfCompany'
HEADER_MAX_HEIGHT = utils.scale(400)
HEADER_MIN_HEIGHT = 0
HEADER_TITLE_HEIGHT = utils.scale(200)
import ItemListDetailFoodOfCompany from '@src/components/ui/ItemListDetailFoodOfCompany'
import BottomBar from '@src/components/ui/BottomBar'
class ListDetailFoodOfCompany extends PureComponent {
    render() {
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp'
        })
        const headerTitleBottom = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0, -HEADER_TITLE_HEIGHT / 2],
            extrapolate: 'clamp'
        })
        const contentMarginTop = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0, HEADER_TITLE_HEIGHT / 2],
            extrapolate: 'clamp'
        })
        const headerZindex = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0, 1000],
            extrapolate: 'clamp'
        })
        const headerColorTabBottom = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_TITLE_HEIGHT / 2, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: ['rgba(0,0,0,0.4)', 'rgba(255,255,255,0)', 'rgba(255,255,255,1)'],
            extrapolate: 'clamp'
        })
        const opacityHeaderNameBottom = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [1, 0],
            extrapolate: 'clamp'
        })
        return (

            <View style={styles.container} >
                <Header
                    iconLeft={'menu'}
                    iconRight={'shopping-cart'}
                />
                <HeaderListDetailFoodOfCompany
                    headerHeight={headerHeight}
                    headerZindex={headerZindex}
                    headerTitleBottom={headerTitleBottom}
                    opacityHeaderNameBottom={opacityHeaderNameBottom}
                    headerColorTabBottom={headerColorTabBottom}
                    data={this.props.navigation.getParam('data')}
                />
                <Animated.FlatList
                    style={{ marginTop: contentMarginTop, }}
                    contentContainerStyle={{
                        paddingTop: HEADER_MAX_HEIGHT + utils.scale(30)
                    }}
                    data={this.props.getDataListDetailFoodCompany.listFood}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => {
                        return (
                            <ItemListDetailFoodOfCompany
                                item={item}
                                lengthData={this.props.getDataListDetailFoodCompany.listFood.length}
                                index={index}
                            />
                        )
                    }
                    }
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
                    )}
                    onEndReached={this.loadMoreData}
                    ListFooterComponent={this.renderActivityIndicator}
                />
                <BottomBar
                    title={'CATEGORIES'}
                    data={'All'}
                />
            </View >
        );
    }
    renderActivityIndicator = () => {
        if (this.props.getIsLoaddingListDetailFoodCompany)
            return (
                <ActivityIndicator
                    color={'red'}
                    style={{ margin: utils.scale(10) }}
                />
            );
        return null
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f1f1'
    },
    image: {
        width: '100%',
        height: utils.deviceHeight / 2
    },
});

export default ListDetailFoodOfCompany;
