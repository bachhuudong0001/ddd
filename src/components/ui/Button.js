import React, { PureComponent } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import * as utils from '@src/utils'
import Icon from "react-native-vector-icons/Feather";

class Button extends PureComponent {
    render() {
        var { title, iconName, iconSize, iconColor, colorText, sizeText, styles } = this.props;
        return (
            <TouchableOpacity
                onPress={this.props.gotoSubmit}
                style={[stylesDefault.btn, styles]}>
                {
                    iconName ?
                        <Icon
                            name={iconName}
                            size={iconSize ? iconSize : utils.scale(40)}
                            color={iconColor ? iconColor : 'white'}
                        />
                        :
                        null
                }
                {
                    title ?
                        <Text style={{
                            color: colorText ? colorText : 'white',
                            fontSize: sizeText ? sizeText : utils.scale(32)
                        }}>{title}</Text>
                        :
                        null
                }
            </TouchableOpacity>
        );
    }
}

const stylesDefault = StyleSheet.create({
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: utils.scale(10),
        height: utils.scale(80),
        width: utils.scale(400),
        backgroundColor: 'transparent',
        marginVertical: utils.scale(20),
        marginHorizontal: utils.scale(20),
    }
});

export default Button;
