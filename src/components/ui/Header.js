import React, { PureComponent } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import * as utils from '@src/utils'
import Icon from "react-native-vector-icons/Feather";

class Header extends PureComponent {
    render() {
        var { iconLeft, iconRight, textLeft, textRight } = this.props;
        return (
            <View style={{ ...styles.container }}>
                <TouchableOpacity onPress={this.props.gotoLeft} style={{ ...styles.btn }}>
                    {
                        iconLeft ?
                            <Icon name={iconLeft} size={utils.scale(40)} color={'white'} />
                            :
                            <Text style={{
                                fontSize: utils.scale(32),
                                color: 'white'
                            }}>{textLeft}</Text>
                    }
                </TouchableOpacity>
                <View style={{ ...styles.containTitle }}>
                    <Icon name={'sun'} size={utils.scale(50)} color={'yellow'} />
                    <Text style={styles.title}>foodapp</Text>
                </View>
                {
                    (iconRight || textRight) ?
                        <TouchableOpacity onPress={this.props.gotoRight} style={{ ...styles.btn }}>
                            {
                                iconRight ?
                                    <Icon name={iconRight} size={utils.scale(40)} color={'white'} />
                                    :
                                    <Text style={{
                                        fontSize: utils.scale(32),
                                        color: 'white',
                                    }}>{textRight}</Text>
                            }
                        </TouchableOpacity>
                        :
                        <View style={{ flex: 1 }} />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        height: utils.scale(100),
        width: utils.scale(100),
        flex: 1
    },
    container: {
        flexDirection: 'row',
        height: utils.scale(100),
        width: '100%',
        alignItems: 'center',
        backgroundColor: 'rgba(236,14,14,0.9)'
    },
    title: {
        fontSize: utils.scale(40),
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        marginLeft: utils.scale(10)
    },
    containTitle: {
        flexDirection: 'row',
        flex: 4,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default Header;
