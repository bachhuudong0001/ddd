import React, { PureComponent } from "react";
import { View, StyleSheet, Text } from "react-native";
import * as utils from '@src/utils'
import Button from '@src/components/ui/Button'

class HeaderListCompanyFood extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <Button
                    iconName={'map-pin'}
                    iconColor={'#999999'}
                    iconSize={utils.scale(40)}
                    styles={{
                        width: utils.scale(80)
                    }}
                />
                <View style={{ flex: 1 }}>
                    <Text style={styles.txtCity} >Lodon</Text>
                    <Text
                        style={styles.txtNameCompany}
                    >Low green str.21-45</Text>
                </View>
                <Button
                    iconName={'edit-3'}
                    iconColor={'#999999'}
                    iconSize={utils.scale(40)}
                    styles={{
                        width: utils.scale(80)
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: utils.scale(5)
    },
    txtCity: {
        textAlign: 'center',
        color: 'red',
        fontSize: utils.scale(28)
    },
    txtNameCompany: {
        textAlign: 'center',
        fontSize: utils.scale(34)
    }
});

export default HeaderListCompanyFood;
