import React, { PureComponent } from "react";
import { View, StyleSheet, Text, Animated, Image } from "react-native";
import * as utils from '@src/utils'
import Button from '@src/components/ui/Button'
import StarRate from "@src/components/ui/StarRate";
HEADER_TITLE_HEIGHT = utils.scale(200)

class HeaderListDetailFoodOfCompany extends PureComponent {

    render() {
        const { headerHeight, headerZindex,
            headerTitleBottom, opacityHeaderNameBottom,
            headerColorTabBottom, data } = this.props;
        return (
            <Animated.View style={{ ...styles.container, height: headerHeight, zIndex: headerZindex }}>
                <Image source={{ uri: data.imageBackground }}
                    style={{ ...styles.image }}
                />
                <Animated.View style={{
                    ...styles.containTitleHeader,
                    bottom: headerTitleBottom,
                    height: HEADER_TITLE_HEIGHT
                }}>
                    <Animated.View opacity={opacityHeaderNameBottom}
                        style={{ ...styles.containTextHeader }}>
                        <Text numberOfLines={1} style={{ ...styles.txtNameCompany }}>{data.nameCompany}</Text>
                        <View style={{ alignItems: 'flex-end', flex: 1, }}>
                            <Text numberOfLines={1} style={{ color: 'white' }}>{data.typeFood}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <StarRate rate={data.rate} />
                                <Text numberOfLines={1} style={{ color: 'white', fontSize: utils.scale(22) }}>(7)</Text>
                            </View>
                        </View>
                    </Animated.View>
                    <Animated.View style={{ ...styles.containTab, backgroundColor: headerColorTabBottom }}>
                        <Button
                            title='Menu'
                            styles={{
                                flex: 1,
                                backgroundColor: 'red'
                            }}
                        />
                        <Button
                            title='Feedback'
                            colorText='red'
                            styles={styles.btntab}
                        />
                        <Button
                            title='Infomation'
                            colorText='red'
                            styles={styles.btntab}
                        />
                    </Animated.View>
                </Animated.View>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: utils.scale(100),
        left: 0,
        right: 0,
    },
    image: {
        flex: 1,
        width: null,
        height: null,
    },
    containTitleHeader: {
        position: 'absolute',
        width: '100%',
        alignItems: 'center',
    },
    containTextHeader: {
        flexDirection: 'row',
        position: 'absolute',
        marginHorizontal: utils.scale(15),
        alignItems: 'center'
    },
    txtNameCompany: {
        flex: 1,
        color: 'white',
        fontSize: utils.scale(46)
    },
    containTab: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        height: HEADER_TITLE_HEIGHT / 2,
        width: '100%',
        padding: 5,
        alignItems: 'center',
        borderBottomWidth: utils.scale(2),
        borderBottomColor: '#e1e1e1'
    },
    btntab: {
        flex: 1,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: 'red'
    }
});

export default HeaderListDetailFoodOfCompany;
