import React, { PureComponent } from "react";
import { View, StyleSheet, Text } from "react-native";
import * as utils from '@src/utils'
import Icon from "react-native-vector-icons/Feather";

class ItemInfoCostDetailCompanyFood extends PureComponent {
    render() {
        var { icon, title, noBorderRight, noPadding } = this.props;
        return (
            <View style={{
                ...styles.container,
                paddingLeft: noPadding ? 0 : utils.scale(20),
            }}>
                <Icon
                    name={icon}
                    size={utils.scale(28)}
                    color={'#d2d2d2'}
                />
                <Text style={{
                    ...styles.txt,
                    paddingRight: noBorderRight ? 0 : utils.scale(40),
                    borderRightWidth: noBorderRight ? 0 : utils.scale(3)
                }}>{title}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    txt: {
        color: '#d2d2d2',
        fontSize: utils.scale(24),
        borderRightColor: '#d2d2d2',
    }
});

export default ItemInfoCostDetailCompanyFood;
