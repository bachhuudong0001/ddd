import React, { PureComponent } from 'react';
import {
    View, Text, ActivityIndicator,
    StyleSheet, TouchableOpacity, Image, FlatList
} from 'react-native'
import * as utils from '@src/utils'
import StarRate from "./StarRate";
import ItemInfoCostDetailCompanyFood from '@src/components/ui/ItemInfoCostDetailCompanyFood'
import AnimationItemList from '@src/components/ui/AnimationItemList'

class ListCompanyFood extends PureComponent {
    renderRow = ({ item, index }) => {
        return (
            <AnimationItemList index={index}>
                <TouchableOpacity onPress={() => this.props.gotoDetail(item)} style={{ ...styles.containerItem }}>
                    <Image
                        style={{ ...styles.imageCompany }}
                        resizeMode='contain'
                        source={{ uri: item.imageCompany }} />
                    <View style={styles.containInfo}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={styles.txtNameCompany}>{item.nameCompany}</Text>
                            <StarRate rate={item.rate} />
                            <Text style={styles.txtStar}>(7)</Text>
                        </View>
                        <Text style={styles.txtTypeFood}>{item.typeFood}</Text>
                        <View style={styles.containInfoCost}>
                            <ItemInfoCostDetailCompanyFood
                                icon={'aperture'}
                                title={`$${item.payShip}`}
                                noPadding
                            />
                            <ItemInfoCostDetailCompanyFood
                                icon={'clock'}
                                title={`${item.timeShip}h`}
                            />
                            <ItemInfoCostDetailCompanyFood
                                icon={'shopping-cart'}
                                title={`MIN$${item.minCost}`}
                                noBorderRight
                            />
                        </View>
                    </View>
                </TouchableOpacity>
            </AnimationItemList>
        )
    }
    render() {
        return (
            <FlatList
                data={this.props.data}
                keyExtractor={item => item.id.toString()}
                renderItem={this.renderRow}
                onEndReached={this.props.loadMoreData}
                ListFooterComponent={() => this.renderActivityIndicator(this.props.getIsLoaddingListFoodCompany)}
            />
        );
    }
    renderActivityIndicator = (isLoadding) => {
        if (isLoadding)
            return (
                <ActivityIndicator
                    color={'red'}
                    style={{ margin: utils.scale(10) }}
                />
            );
        return null
    }

}
const styles = StyleSheet.create({
    containerItem: {
        backgroundColor: 'white',
        borderBottomWidth: utils.scale(3),
        borderBottomColor: '#f3f3f3',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: utils.scale(20)
    },
    imageCompany: {
        height: utils.scale(170),
        width: utils.scale(170),
        borderWidth: utils.scale(2),
        borderColor: '#d2d2d2'
    },
    containInfo: {
        flex: 1,
        marginLeft: utils.scale(20)
    },
    txtNameCompany: {
        flex: 1,
        fontSize: utils.scale(32),
        color: '#444444',
        fontWeight: 'bold'
    },
    txtStar: {
        color: '#d2d2d2',
        fontSize: utils.scale(24)
    },
    txtTypeFood: {
        fontSize: utils.scale(24),
        color: '#a1a1a1'
    },
    containInfoCost: {
        flexDirection: 'row',
        flex: 1,
    }
})
export default ListCompanyFood;
