import React, { PureComponent } from 'react';
import { View, Text, ActivityIndicator, ListView, StyleSheet, TouchableOpacity } from 'react-native'
import * as utils from '@src/utils'
import Icon from "react-native-vector-icons/MaterialIcons";

class ListResultaddress extends PureComponent {
    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.renderRow = this.renderRow.bind(this);
    }

    renderRow(data, sectionId, index) {
        return (
            <TouchableOpacity onPress={() => this.props.gotoDetail(data)} style={{ ...styles.containerItem }}>
                {
                    data.icon ?
                        <Icon name={data.icon} color='red' size={utils.scale(32)} />
                        :
                        null
                }
                {
                    data.title ?
                        <Text style={{
                            color: 'red',
                            marginHorizontal: utils.scale(10),
                            fontSize: utils.scale(30)
                        }}>{data.title}</Text>
                        :
                        null
                }
                <Text
                    style={{
                        color: data.address && data.title ? '#d2d2d2' : '#333333',
                        fontSize: utils.scale(28),
                        flex: 1
                    }}

                >{data.address && data.title ? `(${data.address})` : data.address}</Text>
            </TouchableOpacity>
        )
    }
    render() {
        var { dataMyLocation, dataResultLocation } = this.props;
        var dataSource = this.ds.cloneWithRows(
            utils.joinDataListResultLocationAndMyLocation(dataMyLocation, dataResultLocation));
        return (
            <ListView
                showsHorizontalScrollIndicator={false}
                enableEmptySections={true}
                dataSource={dataSource}
                renderRow={this.renderRow}
                renderFooter={this.renderActivityIndicator}
                keyboardShouldPersistTaps='handled'
            />
        );
    }
    renderActivityIndicator = () => {
        if (this.props.getIsLoaddingListFoodCompanySearchByname)
            return (
                <ActivityIndicator
                    color={'red'}
                    style={{ margin: utils.scale(10) }}
                />
            );
        return null
    }

}
const styles = StyleSheet.create({
    containerItem: {
        backgroundColor: 'white',
        borderBottomWidth: utils.scale(2),
        borderBottomColor: '#f3f3f3',
        flexDirection: 'row',
        alignItems: 'center',
        padding: utils.scale(20)
    }
})
export default ListResultaddress;
