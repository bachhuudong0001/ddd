import React, { PureComponent } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import * as utils from '@src/utils'
import Icon from "react-native-vector-icons/MaterialIcons";
const star = [1, 2, 3, 4, 5]
class StarRate extends PureComponent {
    render() {
        return (
            <View style={{ ...styles.container }}>
                {
                    star.map((data, index) => {
                        return (
                            <TouchableOpacity key={data}>
                                <Icon
                                    name='star'
                                    size={utils.scale(28)}
                                    color={this.props.rate >= data ? '#FFF254' : '#d2d2d2'}
                                />
                            </TouchableOpacity>
                        )
                    })
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: utils.scale(50),
        alignItems: 'center'
    },

});

export default StarRate;
