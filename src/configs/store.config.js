import {
  createStore,
  applyMiddleware,
  compose,
} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducers from './reducers.config';
import rootSagas from '../sagas';
import * as actions from '../actions';

const env = process.env.NODE_ENV;
const sagaMiddleware = createSagaMiddleware();
const middlewares = sagaMiddleware;

const configureStore = () => {
  let enhancers;

  if (env !== 'production') {
    // development env - enable dev tools
    const composeWithDevTools = require('redux-devtools-extension').composeWithDevTools;
    enhancers = composeWithDevTools(
      applyMiddleware(middlewares),
    );
  } else {
    // production env - exclude dev tools
    enhancers = compose(
      applyMiddleware(middlewares),
    );
  }

  const store = createStore(
    rootReducers,
    enhancers
  );

  sagaMiddleware.run(rootSagas);
  store.dispatch(actions.loadInitialStateRequest());
  return store;
};

export default configureStore();
