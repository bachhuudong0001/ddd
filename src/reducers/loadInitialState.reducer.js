import * as type from '../actionTypes';
import * as utils from '../utils';
const initialState = {
    dataListRecommend: [],
    dataInfoLocaionUser: []
};

export default function loadInitialState(state = initialState, action) {
    switch (action.type) {
        case type.LOAD_INITIAL_STATE_SUCCESS:
            return {
                ...state,
                dataListRecommend: action.result.resultTakeListRecommend,
                dataInfoLocaionUser: action.result.resultInfoLocaionUser,
            };
        default:
            return state;
    }
}
