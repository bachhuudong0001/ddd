import * as type from '../actionTypes';
import * as utils from '../utils';
const initialState = {
    isLoaddingListDetailFoodCompany: false,
    dataListDetailFoodCompany: []
};

export default function takeDataDetailFoodCompany(state = initialState, action) {
    switch (action.type) {
        case type.TAKE_DATA_DETAIL_FOOD_COMPANY_REQUEST:
            return {
                ...state,
                isLoaddingListDetailFoodCompany: true
            };
        case type.TAKE_DATA_DETAIL_FOOD_COMPANY_SUCCESS:
            return {
                ...state,
                isLoaddingListDetailFoodCompany: false,
                dataListDetailFoodCompany: action.result,
            };
        case type.RESET_TAKE_DATA_DETAIL_FOOD_COMPANY:
            return {
                ...state,
                isLoaddingListDetailFoodCompany: false,
                dataListDetailFoodCompany: [],
            };
        default:
            return state;
    }
}
