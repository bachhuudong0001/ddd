import { call, put, takeLatest } from 'redux-saga/effects';
import * as types from '@src/actionTypes';
import * as actions from '@src/actions';
import * as utils from '@src/utils'


function* findLocationFoodCompany(action) {
    try {
        const result = yield call(utils.takeAddressByName, action.payload);
        if (result) {
            yield put(actions.findLocationFoodCompanySuccess(result))
        }
        else {
            yield put(actions.findLocationFoodCompanyFailed("failed load data"))
        }
    } catch (error) {
        yield put(actions.findLocationFoodCompanyFailed(error))
    }
}

function* listLocationFoodCompany(action) {
    try {
        const result = yield call(utils.takeListRecommend, 1000);
        if (result) {
            yield put(actions.listLocationFoodCompanySuccess(result))
        }
        else {
            yield put(actions.listLocationFoodCompanyFailed("failed load data"))
        }
    } catch (error) {
        yield put(actions.listLocationFoodCompanyFailed(error))
    }
}

export function* watchFindLocationFoodCompany() {
    yield takeLatest(types.FIND_LOCATION_FOOD_COMPANY_REQUEST, findLocationFoodCompany);
}

export function* watchListLocationFoodCompany() {
    yield takeLatest(types.LIST_LOCATION_FOOD_COMPANY_REQUEST, listLocationFoodCompany);
}
