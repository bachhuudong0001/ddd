import { call, put, takeLatest } from 'redux-saga/effects';
import * as types from '@src/actionTypes';
import * as actions from '@src/actions';
import * as utils from '@src/utils'


function* loadInitialStateFlow() {
  try {
    const resultTakeListRecommend = yield call(utils.takeListRecommend,0);
    const resultInfoLocaionUser = yield call(utils.takeInfoLocaionUser);
    if (resultTakeListRecommend || resultInfoLocaionUser) {
      yield put(actions.loadInitialStateSuccess({ resultTakeListRecommend, resultInfoLocaionUser }))
    }
    else {
      yield put(actions.loadInitialStateFailed("failed load data"))
    }
  } catch (error) {
    yield put(actions.loadInitialStateFailed(error))
  }
}

export function* loadInitialState() {
  yield takeLatest(types.LOAD_INITIAL_STATE_REQUEST, loadInitialStateFlow);
}
