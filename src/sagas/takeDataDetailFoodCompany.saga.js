import { call, put, takeLatest } from 'redux-saga/effects';
import * as types from '@src/actionTypes';
import * as actions from '@src/actions';
import * as utils from '@src/utils'


function* takeDataDetailFoodCompany(action) {
    try {
        const result = yield call(utils.takeDataDetailFoodFromCompany, action.payload);
        console.log("result", result)
        if (result) {
            yield put(actions.takeDataDetailFoodCompanySuccess(result))
        }
        else {
            yield put(actions.takeDataDetailFoodCompanyFailed("failed load data"))
        }
    } catch (error) {
        yield put(actions.takeDataDetailFoodCompanyFailed(error))
    }
}


export function* watchTakeDataDetailFoodCompany() {
    yield takeLatest(types.TAKE_DATA_DETAIL_FOOD_COMPANY_REQUEST, takeDataDetailFoodCompany);
}
