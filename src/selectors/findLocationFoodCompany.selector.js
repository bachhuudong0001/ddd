export const getDataListFoodCompanySearchByname = (state) => state.findLocationFoodCompany.dataListFoodCompanySearchByname;
export const getIsLoaddingListFoodCompanySearchByname = (state) => state.findLocationFoodCompany.isLoaddingListFoodCompanySearchByname;

export const getDataListFoodCompany = (state) => state.findLocationFoodCompany.dataListFoodCompany;
export const getIsLoaddingListFoodCompany = (state) => state.findLocationFoodCompany.isLoaddingListFoodCompany;
