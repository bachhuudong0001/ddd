
import { createStackNavigator } from "react-navigation";
import LaunchScreen from "../components/app/LaunchScreen";
import HomeScreen from "../components/app/HomeScreen.container";
import ListDetailFoodOfCompany from "../components/app/ListDetailFoodOfCompany.container";
import ListCompanyFoodScreen from "../components/app/ListCompanyFoodScreen.container";


const CreateStackNavigator = createStackNavigator(
    {
        LaunchScreen: {
            screen: LaunchScreen
        },
        HomeScreen: {
            screen: HomeScreen
        },
        ListCompanyFoodScreen: {
            screen: ListCompanyFoodScreen
        },
        ListDetailFoodOfCompany: {
            screen: ListDetailFoodOfCompany
        }
    },
    {
        initialRouteName: 'HomeScreen',
        navigationOptions: {
            header: null,
            gesturesEnabled: true,
        },
        transitionConfig: () => ({
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;

                const translateX = position.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [layout.initWidth, 0, 0]
                });

                const opacity = position.interpolate({
                    inputRange: [
                        index - 1,
                        index - 0.99,
                        index,
                        index + 0.99,
                        index + 1
                    ],
                    outputRange: [0, 1, 1, 0.3, 0]
                });

                return { opacity, transform: [{ translateX }] };
            }
        })
    }
);

export default CreateStackNavigator;