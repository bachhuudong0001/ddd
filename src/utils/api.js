const dataCompany = require('@src/assets/database/dataCompany.json');
const dataMyLocation = require('@src/assets/database/dataMyLocation.json');

export function takeListRecommend(timeLoad) {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res(dataCompany);
        }, timeLoad);
    })

}

export function takeInfoLocaionUser() {
    return new Promise((res, rej) => {
        res(dataMyLocation);
    })

}

export function takeAddressByName(address) {
    return new Promise((res, rej) => {
        if (address) {
            var dataEx = []
            for (let i = 0; i <= dataCompany.length; i++) {
                if (i == dataCompany.length) {
                    res(dataEx)
                }
                else {
                    checkLikeNameAddress(address, dataCompany[i].address).then((result) => {
                        if (result != -1)
                            dataEx.push({
                                id: dataCompany[i].id,
                                address: dataCompany[i].address
                            });
                    })
                }
            }

        }
        else {
            res([])
        }
    })

}
function checkLikeNameAddress(address, addressCompany) {
    return new Promise((res, rej) => {
        res(parseInt(addressCompany.toLowerCase().search(address.toLowerCase())))//search %like% text
    })
}

export function takeDataDetailFoodFromCompany(id) {
    return new Promise((res, rej) => {
        setTimeout(() => {
            for (let i = 0; i < dataCompany.length; i++) {
                if (dataCompany[i].id == id)
                    res(dataCompany[i]);
            }
        }, 1000);
    })

}