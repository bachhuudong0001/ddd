import { Platform, Dimensions } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const platform = Platform.OS;
const guidelineBaseWidth = 750;
const scale = (size) => deviceWidth / guidelineBaseWidth * size;

export {
    platform,
    scale,
    deviceWidth,
    deviceHeight
};
